#!/usr/bin/env python

__version__ = '1.0'
__author__ = 'Damian Gorzynski'
__email__ = 'damian10012@gmail.com'
__copyright__ = "Copyright (c) 2020 Damian Gorzynski"

class Cube:
    def __init__(self, capacity: int):
        self.capacity: int = capacity

    def __gt__(self, other):
        return self.capacity > other.capacity

    def __lt__(self, other):
        return self.capacity < other.capacity

    def __eq__(self, other):
        return self.capacity == other.capacity

    def __ge__(self, other):
        return self.capacity >= other.capacity

    def __le__(self, other):
        return self.capacity <= other.capacity

    def __ne__(self, other):
        return self.capacity != other.capacity

a = Cube(8 ** 3)
b = Cube(6 ** 3)

print(format(a > b))
print(format(a < b))
print(format(a == b))
print(format(a >= b))
print(format(a <= b))
print(format(a != b))