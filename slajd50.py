#!/usr/bin/env python

__version__ = '1.1'
__author__ = 'Damian Gorzynski'
__email__ = 'damian10012@gmail.com'
__copyright__ = "Copyright (c) 2020 Damian Gorzynski"


class Shop:
    art = 450

    def show_shop_name(self):
        print("Żabka")


class Milk(Shop):
    def __init__(self):
        print("Number of articles:", Shop.art)


class Apple(Shop):
    def __init__(self):
        self.show_shop_name()


class Newspaper(Shop):
    def show_shop_name(self):
        print("Do shopping at Żabka")
        super(Newspaper, self).show_shop_name()
