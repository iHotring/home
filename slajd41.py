#!/usr/bin/env python

__version__ = '1.0'
__author__ = 'Damian Gorzynski'
__email__ = 'damian10012@gmail.com'
__copyright__ = "Copyright (c) 2020 Damian Gorzynski"

def print_my_name(name: str) -> None:
    print("My name is:", name)

print_my_name("Damian")


def add_3_numbers(a: int, b: int, c: int) -> int:
    print(a+b+c)

add_3_numbers(5, 4, 5)


def hello() -> str:
    return("Hello world")

print(hello())


def sub_2_numbers(a: int, b: int) -> int:
    return a-b

print(sub_2_numbers(6, 3))



class Book:
    pass

def create_book() -> Book:
    return Book()

horror = create_book()
print(isinstance(horror, Book))
print(horror)

