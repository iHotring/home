#!/usr/bin/env python

__version__ = '1.1'
__author__ = 'Damian Gorzynski'
__email__ = 'damian10012@gmail.com'
__copyright__ = "Copyright (c) 2020 Damian Gorzynski"


class VOD:
    def __init__(self, season: int, episode: int):
        self.season: int = season
        self.episode: int = episode

    def __gt__(self, other):
        return self.season > other.season and self.episode > other.episode

    def __lt__(self, other):
        return self.season < other.season and self.episode < other.episode

    def __eq__(self, other):
        return self.season == other.season and self.episode == other.episode

    def __ge__(self, other):
        return self.season >= other.season and self.episode >= other.episode

    def __le__(self, other):
        return self.season <= other.season and self.episode <= other.episode

    def __ne__(self, other):
        return self.season != other.season and self.episode != other.episode


class Fantasy(VOD):
    def __init__(self, season: int, episode: int):
        super(Fantasy, self).__init__(season, episode)


class Horror(VOD):
    def __init__(self, season: int, episode: int):
        super(Horror, self).__init__(season, episode)


class Game_of_Thrones(Fantasy):
    def __init__(self, season: int, episode: int):
        super(Game_of_Thrones, self).__init__(season, episode)


class Witcher(Fantasy):
    def __init__(self, season: int, episode: int):
        super(Witcher, self).__init__(season, episode)


class The_Walking_Dead(Horror):
    def __init__(self, season: int, episode: int):
        super(The_Walking_Dead, self).__init__(season, episode)


class Slasher(Horror):
    def __init__(self, season: int, episode: int):
        super(Slasher, self).__init__(season, episode)


Four_Marks = Witcher(1, 2)
JSS = Horror(6, 2)
The_Climb = VOD(5, 3)

print(format(Four_Marks == JSS))
print(format(JSS == The_Climb))
print(format(The_Climb == Four_Marks))
