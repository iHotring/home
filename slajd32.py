#!/usr/bin/env python

__version__ = '1.0'
__author__ = 'Damian Gorzynski'
__email__ = 'damian10012@gmail.com'
__copyright__ = "Copyright (c) 2020 Damian Gorzynski"

class VOD:
    pass

class Netflix(VOD):
    pass

class The100(Netflix):
    pass