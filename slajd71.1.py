#!/usr/bin/env python

__version__ = '1.0'
__author__ = 'Damian Gorzynski'
__email__ = 'damian10012@gmail.com'
__copyright__ = "Copyright (c) 2020 Damian Gorzynski"

name = input("Give me your name: ")

if name in "Damian":
    print("Hello Damian!")
else:
    print("Goodbye!")