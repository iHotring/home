#!/usr/bin/env python

__version__ = '1.1'
__author__ = 'Damian Gorzynski'
__email__ = 'damian10012@gmail.com'
__copyright__ = "Copyright (c) 2020 Damian Gorzynski"


class Rectangle:
    def __init__(self, side_length: int, side_width: int):
        self.side_length: int = side_length
        self.side_width: int = side_width

    def __gt__(self, other):
        return self.side_length > other.side_length and self.side_width > other.side_width

    def __lt__(self, other):
        return self.side_length < other.side_length and self.side_width < other.side_width

    def __eq__(self, other):
        return self.side_length == other.side_length and self.side_width == other.side_width

    def __ge__(self, other):
        return self.side_length >= other.side_length and self.side_width >= other.side_width

    def __le__(self, other):
        return self.side_length <= other.side_length and self.side_width <= other.side_width

    def __ne__(self, other):
        return self.side_length != other.side_length and self.side_width != other.side_width


a = Rectangle(5, 7)
b = Rectangle(8, 7)

print(format(a > b))
print(format(a < b))
print(format(a == b))
print(format(a >= b))
print(format(a <= b))
print(format(a != b))
