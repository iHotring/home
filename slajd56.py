#!/usr/bin/env python

__version__ = '1.0'
__author__ = 'Damian Gorzynski'
__email__ = 'damian10012@gmail.com'
__copyright__ = "Copyright (c) 2020 Damian Gorzynski"

name = input("Give me your name: ")
surname = input("Give me your surname: ")
age = input("Give me your age: ")

print(name, surname, int(age))


def number():
    enter_number = input("\nGive me a number: ")
    print(int(enter_number))

number()

class Violet:
    def __init__(self, price: float, color: str):
        print("The flower costs", float(price), "and has a color", color)

price = input("\nEnter flower price: ")
color = input("Enter flower color: ")

violet = Violet(price, color)