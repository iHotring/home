#!/usr/bin/env python

__version__ = '1.0'
__author__ = 'Damian Gorzynski'
__email__ = 'damian10012@gmail.com'
__copyright__ = "Copyright (c) 2020 Damian Gorzynski"

objects = [
    [2],
    [1],
    [3]
]

a = list(i for i in range(100, 116))
print(a)

float_list = [2.14, 4.32, 6.67, 3.14, 1.11]
for i in float_list:
    print(i)