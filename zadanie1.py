#!/usr/bin/env python

__version__ = '1.0'
__author__ = 'Damian Gorzynski'
__email__ = 'damian10012@gmail.com'
__copyright__ = "Copyright (c) 2020 Damian Gorzynski"


"""Zadanie domowe ze slajdu 22"""

age = int(22)
print("I'm", age, "years old")

height = float(1.74)
print("I'm", height, "meters tall")

name = str("Damian Górzyński")
print("My name is", name,"\n")

"""Zadanie domowe ze slajdu 29"""

class Plants:
    pass

class Perfumes:
    pass

class Cakes:
    pass


violet = Plants()
chanel = Perfumes()
gingerbread = Cakes()

print(violet, chanel, gingerbread)